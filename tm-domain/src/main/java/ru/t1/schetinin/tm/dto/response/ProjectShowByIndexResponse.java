package ru.t1.schetinin.tm.dto.response;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.dto.model.ProjectDTO;

@Getter
@Setter
@NoArgsConstructor
public class ProjectShowByIndexResponse extends AbstractProjectResponse {

    public ProjectShowByIndexResponse(@Nullable final ProjectDTO project) {
        super(project);
    }

}