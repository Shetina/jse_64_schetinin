package ru.t1.schetinin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.schetinin.tm.dto.request.TaskRemoveByIdRequest;
import ru.t1.schetinin.tm.event.ConsoleEvent;
import ru.t1.schetinin.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIdListener extends AbstractTaskListener {

    @NotNull
    private static final String NAME = "task-remove-by-id";

    @NotNull
    private static final String DESCRIPTION = "Remove task by id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskRemoveByIdListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(getToken());
        request.setId(id);
        taskEndpoint.removeTaskById(request);
    }

}