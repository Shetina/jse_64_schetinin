## TASK MANAGER

## DEVELOPER INFO

* **Name**: Nikita Schetinin

* **E-mail**: shetinin86@bk.ru

* **E-mail**: shetina-shesh@bk.ru

## SOFTWARE

* **OS**: Windows 10

* **JAVA**: OpenJDK 1.8.0_345

## HARDWARE

* **CPU**: AMD Ryzen 5 5600X

* **RAM**: 16GB

* **SSD**: 512GB

## PROGRAM BUILD

```shell
mvn clean install
```

## PROGRAM RUN

```shell
java -jar ./task-manager.jar
```