package ru.t1.schetinin.tm.repository.model;

import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.schetinin.tm.model.AbstractUserOwnedModel;

@NoRepositoryBean
public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

}