package ru.t1.schetinin.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.schetinin.tm.api.service.dto.IProjectDTOService;
import ru.t1.schetinin.tm.dto.model.ProjectDTO;
import ru.t1.schetinin.tm.enumerated.TMSort;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.schetinin.tm.exception.field.DescriptionEmptyException;
import ru.t1.schetinin.tm.exception.field.IdEmptyException;
import ru.t1.schetinin.tm.exception.field.NameEmptyException;
import ru.t1.schetinin.tm.exception.field.StatusEmptyException;
import ru.t1.schetinin.tm.exception.user.UserIdEmptyException;
import ru.t1.schetinin.tm.repository.dto.IProjectDTORepository;

import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectDTOService extends AbstractUserOwnedDTOService<ProjectDTO> implements IProjectDTOService {

    @NotNull
    @Autowired
    private IProjectDTORepository repository;

    @NotNull
    protected IProjectDTORepository getRepository() {
        return repository;
    }

    @Override
    @Transactional
    public void changeProjectStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        repository.save(project);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO create(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
        return project;
    }

    @Override
    @Transactional
    public void updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
    }

    @Override
    @Nullable
    public List<ProjectDTO> findAll(@Nullable String userId, @Nullable TMSort sort) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        @Nullable final Sort findSort;
        switch (sort.name()) {
            case "BY_NAME":
                findSort = Sort.by(Sort.Direction.ASC, "name");
                break;
            case "BY_STATUS":
                findSort = Sort.by(Sort.Direction.ASC, "status");
                break;
            default:
                findSort = Sort.by(Sort.Direction.ASC, "created");
        }
        return repository.findByUserId(userId, findSort);
    }

    @Override
    public void removeById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public void clear(@NotNull String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserId(userId);
    }

    @Override
    public boolean existsById(@NotNull String userId, @NotNull String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        return repository.existsByUserIdAndId(userId, id);
    }

    @Override
    @Nullable
    public List<ProjectDTO> findAll(@NotNull String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findByUserId(userId);
    }

    @Override
    @Nullable
    public ProjectDTO findOneById(@NotNull String userId, @NotNull String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Optional<ProjectDTO> result = repository.findByUserIdAndId(userId, id);
        return result.orElse(null);
    }

    @Override
    public int getSize(@NotNull String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return (int) repository.countByUserId(userId);
    }

}