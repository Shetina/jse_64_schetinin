package ru.t1.schetinin.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.t1.schetinin.tm.api.service.model.ISessionService;
import ru.t1.schetinin.tm.api.service.model.IUserService;
import ru.t1.schetinin.tm.enumerated.TMSort;
import ru.t1.schetinin.tm.exception.field.IdEmptyException;
import ru.t1.schetinin.tm.exception.user.UserIdEmptyException;
import ru.t1.schetinin.tm.model.Session;
import ru.t1.schetinin.tm.repository.model.ISessionRepository;

import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
public class SessionService extends AbstractUserOwnedService<Session> implements ISessionService {

    @NotNull
    @Autowired
    private ISessionRepository repository;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    protected ISessionRepository getRepository() {
        return repository;
    }

    @Override
    @Nullable
    public List<Session> findAll(@Nullable String userId, @Nullable TMSort sort) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        @Nullable final Sort findSort;
        switch (sort.name()) {
            case "BY_NAME":
                findSort = Sort.by(Sort.Direction.ASC, "name");
                break;
            case "BY_STATUS":
                findSort = Sort.by(Sort.Direction.ASC, "status");
                break;
            default:
                findSort = Sort.by(Sort.Direction.ASC, "created");
        }
        return repository.findByUser(userService.findOneById(userId), findSort);
    }

    @Override
    public void removeById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteByUserAndId(userService.findOneById(userId), id);
    }

    @Override
    public void clear(@NotNull String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUser(userService.findOneById(userId));
    }

    @Override
    public boolean existsById(@NotNull String userId, @NotNull String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        return repository.existsByUserIdAndId(userId, id);
    }

    @Override
    @Nullable
    public List<Session> findAll(@NotNull String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findByUser(userService.findOneById(userId));
    }

    @Override
    @Nullable
    public Session findOneById(@NotNull String userId, @NotNull String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Optional<Session> result = repository.findByUserAndId(userService.findOneById(userId), id);
        return result.orElse(null);
    }

    @Override
    public int getSize(@NotNull String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return (int) repository.countByUser(userService.findOneById(userId));
    }

}