package ru.t1.schetinin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import ru.t1.schetinin.tm.model.Session;
import ru.t1.schetinin.tm.model.User;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface ISessionRepository extends IUserOwnedRepository<Session> {

    long countByUser(@NotNull final User user);

    void deleteByUserAndId(@NotNull final User user, @NotNull final String id);

    @NotNull
    List<Session> findByUser(@NotNull final User user);

    @NotNull
    List<Session> findByUser(@NotNull final User user, @NotNull final Sort sort);

    @NotNull
    Optional<Session> findByUserAndId(@NotNull final User user, @NotNull final String id);

    void deleteByUser(@NotNull final User user);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

}